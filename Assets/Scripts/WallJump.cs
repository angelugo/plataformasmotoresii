﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WallJump : Controller_Player
{
   
    public int cantMaxSaltos;
    public int jumpCounter;
    public int trampolineForce;
    
    
    public override void Jump()
    {
        if (onWall)
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                saltoActual = 1;
                Rigidbody rigidbody = gameObject.GetComponent<Rigidbody>();
                rigidbody.AddForce(new Vector3(0, 5 * trampolineForce, 0), ForceMode.Impulse);

            }
        }
        else
        {
            if (Input.GetKeyDown(KeyCode.W))
            {
                if (saltoActual > 0)
                {
                    rb.AddForce(new Vector3(0, 5, 0), ForceMode.Impulse);
                    saltoActual--;
                   
                }
            }
        }
    }
 

}
