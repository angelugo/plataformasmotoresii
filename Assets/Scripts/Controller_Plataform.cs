﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Plataform : MonoBehaviour
{
    public Transform[] target;
    public GameObject punto1;
    public GameObject punto0;
    public Vector3 poss1;
    public Vector3 poss0;
    public Vector3 PossPalyer;
    public float velocidad;
    public Rigidbody rb;
    public GameObject Player;
    public bool Push;
    public int CurPos=0;
    public bool MoveNex;
    public int NexPos=1;
    public int NumPress;
    
    public int NumMax;
    private void Update()
    {
        poss1 = Vector3.zero;
        PossPalyer = Vector3.zero;
        if (Input.GetKeyDown(KeyCode.G))
        {
            NumPress += 1;
            if (NumPress < NumMax)
            {
                Push = true;
                Player.GetComponent<Rigidbody>().useGravity = false;
            }
            else
            {
                Push = false;

                Player.GetComponent<Rigidbody>().useGravity = true;
                NumPress = 0;
            }
          
        }
        PossPalyer = new Vector3(Player.transform.position.x, Player.transform.position.y, Player.transform.position.z);

        poss1.x = PossPalyer.x;
        punto1.transform.position = new Vector3(poss1.x, punto1.transform.position.y, punto1.transform.position.z);

        poss0.x = PossPalyer.x;
        poss0.y = PossPalyer.y;
        punto0.transform.position = new Vector3(poss0.x, punto0.transform.position.y, punto0.transform.position.z);



    }
    private void FixedUpdate()
    {
        if (Push == true)
        {
            Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;
            if(MoveNex)
            transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
                if (Vector3.Distance(transform.position, target[NexPos].position) <= 0)
                {
               StartCoroutine(MoveTime());
                CurPos = NexPos;
                    NexPos++;

                    if (NexPos > target.Length - 1)
                    {
                        NexPos = 0;
                    }
                }
            
           

        }
        else
        {
    
            Player.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
        }
      
      
    }
   IEnumerator MoveTime()
    {
        MoveNex = false;
        yield return new WaitForSeconds(2);
        MoveNex = true;

    }

}
