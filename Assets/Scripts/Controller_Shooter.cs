﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Shooter : MonoBehaviour
{
    public GameObject bulletPrefab;
    public float bulletForce = 20f;
    public Transform firePoint;

    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetButtonDown("Fire1"))
        {
            GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);
            Rigidbody rb = bullet.GetComponent<Rigidbody>();
            rb.AddForce(firePoint.forward * bulletForce, ForceMode.Impulse);
            Destroy(bullet, 1.5f);
        }
    }
}
