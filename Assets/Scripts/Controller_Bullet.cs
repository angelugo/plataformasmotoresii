﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Bullet : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(gameObject);
        }
        if (other.gameObject.CompareTag("Enemy"))
        {
            Destroy(gameObject);
            Destroy(other.gameObject);

        }
        if (other.gameObject.CompareTag("Teleport"))
        {
            Destroy(gameObject);
            Destroy(other.gameObject);

        }

    }
    
}
