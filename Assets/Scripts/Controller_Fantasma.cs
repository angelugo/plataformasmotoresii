﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_Fantasma : MonoBehaviour
{
    public int NumPress;
    public int NumMax;
    public Material Mat1;
    public Material Mat2;
 
    void Start()
    {
      
    }

    // Update is called once per frame
    void Update()
    {
        Renderer rend = gameObject.GetComponent<Renderer>();
        if (Input.GetKeyDown(KeyCode.F))
        {
            NumPress += 1;
            if (NumPress < NumMax)
            {
                rend.material = Mat2;
                gameObject.GetComponent<BoxCollider>().isTrigger = true;
                gameObject.GetComponent<Rigidbody>().useGravity = false;
                gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionY | RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                rend.material = Mat1;

                gameObject.GetComponent<BoxCollider>().isTrigger = false;
                gameObject.GetComponent<Rigidbody>().useGravity = true;
                gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
                NumPress = 0;
            }
          
        }
    }
}
