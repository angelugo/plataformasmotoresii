﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controller_LongPlayer : MonoBehaviour
{
    public int NumPress;
    public int NumMax;
    public Transform ScaleInitial;
    void Start()
    {

    }

    void Update()
    {
        if (Input.GetKeyDown(KeyCode.T))
        {
            NumPress += 1;

            if (NumPress <NumMax)
            {
                transform.localScale = new Vector3(transform.localScale.x + 8, transform.localScale.y, transform.localScale.z);

            }
            else
            {
                
                transform.localScale = new Vector3(transform.localScale.x -8, transform.localScale.y, transform.localScale.z);
                NumPress = 0;
            }
        }
    }
}
