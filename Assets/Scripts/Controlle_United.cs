﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Controlle_United : MonoBehaviour
{
    public Transform United1;
    public GameObject Player1;
    public Controller_Player Jugador;
    public int NumPress;
    public int NumMax;
    public GameObject Rb;
    public GameObject Zona;
    public bool InZone;
    public bool inParent;

    void Start()
    {
        Jugador = GameObject.Find("Player9").GetComponent<Controller_Player>();

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.F)&&InZone)
        {
            NumPress += 1;
            if (NumPress<NumMax)
            {
                Player1.transform.SetParent(United1);
               Player1.transform.position = United1.transform.position;
               Rb.GetComponent<Rigidbody>().useGravity = false;
          
                inParent = true;
                Rb.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition| RigidbodyConstraints.FreezeRotation;
            }
            else
            {
                Zona.SetActive(true);
                Player1.transform.SetParent(null);
                Player1.transform.position = United1.transform.position;

                Rb.GetComponent<Rigidbody>().useGravity = true;
           
                inParent = false;


                NumPress = 0;

            }
            if (inParent )
            {
                Rb.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePosition | RigidbodyConstraints.FreezeRotation;

            }
            else
            {
                Rb.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezePositionZ | RigidbodyConstraints.FreezeRotation;
            }

        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ZonaUni1"))
        {
            InZone = true;
        }

    }
    private void OnTriggerExit(Collider other)
    {

        if (other.gameObject.CompareTag("ZonaUni1"))
        {
            InZone = false;
        }
    }
}
