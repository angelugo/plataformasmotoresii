﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Destruir : MonoBehaviour
{
    public GameObject Enemy;
    public float trampolineForce;
    public GameObject Player;
    private void Start()
    {
        Player = GameObject.Find("Player11");
        trampolineForce = 2;
    }
    // Start is called before the first frame update
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("ZonaMatar"))
        {
            Rigidbody rigidbody = Player.gameObject.GetComponent<Rigidbody>();
            rigidbody.AddForce(new Vector3(0, 10 * trampolineForce, 0), ForceMode.Impulse);
            Destroy(Enemy);
         
         
        }
     

    }
   
}
