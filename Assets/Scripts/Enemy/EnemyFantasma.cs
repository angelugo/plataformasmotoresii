﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyFantasma : MonoBehaviour
{
    public Material Mat1;
    public Material Mat2;
    public Renderer rend;
    public GameObject[] Player;
    public float speed;
    public int refe;
    public bool ModoFantasma;
    public GameObject ZonaFantasma;
    // Start is called before the first frame update
    void Start()
    {
        rend = gameObject.GetComponent<Renderer>();
        Player = GameObject.FindGameObjectsWithTag("Player");

    }

    // Update is called once per frame
    void Update()
    {

        refe = GameManager.actualPlayer;
        if (Player[refe] != null )
        {
            if (Vector3.Distance(transform.position, Player[refe].transform.position) <= 10)
            {
                float step = speed * Time.deltaTime;

                transform.position = Vector3.MoveTowards(transform.position, Player[refe].transform.position, step);
                StartCoroutine(Fantasma());
            }
        }
       
        if (ModoFantasma)
        {
            rend.material = Mat2;
            gameObject.GetComponent<BoxCollider>().isTrigger = true;
            gameObject.tag = "Fantasma";
            ZonaFantasma.SetActive(true);
        }
        else
        {
            rend.material = Mat1;
            gameObject.GetComponent<BoxCollider>().isTrigger = false;
            gameObject.tag = "Enemy";
            ZonaFantasma.SetActive(false);
        }
    }
    IEnumerator Fantasma()
    {
        ModoFantasma = true;
        yield return new WaitForSeconds(2);
        ModoFantasma = false;

    }
}