﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DestruirObstaculo : MonoBehaviour
{
    public GameObject Obstaculo;

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            Destroy(Obstaculo);

        }
    }
}
