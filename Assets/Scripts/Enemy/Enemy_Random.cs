﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy_Random : MonoBehaviour
{
    public int Numero;
    public GameObject Enemigo;
    // Start is called before the first frame update
    void Start()
    {
        Numero = Random.Range(1, 4);
    }

    // Update is called once per frame
    void Update()
    {
      
    }
  
    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Bala"))
        {
            Destroy(gameObject);
            Destroy(other.gameObject);
            if (Numero == 2 || Numero == 4 )
            {
                Instantiate(Enemigo, transform.position, Quaternion.identity);
            }
        }
    }
}
