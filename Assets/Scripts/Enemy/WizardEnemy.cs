﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WizardEnemy : MonoBehaviour
   
{
    public Transform[] target;
    public int CurPos = 0;
    public int NexPos = 1;
    public bool Push;
    public float velocidad;
    public GameObject[] player1;
    public int numeroDeEnemigos;
    public Transform firePoint;
    public GameObject bulletPrefab;
    public NavMeshAgent agent;
    public int NumeroDeEnemigosLimite;
    public int refe;

    public void Start()
    {
        player1 = GameObject.FindGameObjectsWithTag("Player");

    }
    void Update()
    {
        refe = GameManager.actualPlayer;
        if (player1[refe] != null)
        {
            if (Vector3.Distance(player1[refe].transform.position, transform.position) < 6 && numeroDeEnemigos < NumeroDeEnemigosLimite)
            {

                numeroDeEnemigos += 1;
                transform.LookAt(player1[refe].transform.position);
                GameObject bullet = Instantiate(bulletPrefab, firePoint.position, firePoint.rotation);




            }
        }
        if (Push == false)
        {
            transform.position = Vector3.MoveTowards(transform.position, target[NexPos].transform.position, velocidad * Time.deltaTime);
            if (Vector3.Distance(transform.position, target[NexPos].position) <=0 )
            {

                CurPos = NexPos;
                NexPos++;

                if (NexPos > target.Length - 1)
                {
                    NexPos = 0;
                }
            }
        }

    }
  
}
