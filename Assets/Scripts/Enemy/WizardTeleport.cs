﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class WizardTeleport : MonoBehaviour
{
    public GameObject[] Player;
    public int refe;
    public Transform Punto;
    public NavMeshAgent agent;
    // Start is called before the first frame update
    void Start()
    {
        Player = GameObject.FindGameObjectsWithTag("Player");
    }

    // Update is called once per frame
    void Update()
    {
        refe = GameManager.actualPlayer;
        if (Player[refe] != null)
        {
            if (Vector3.Distance(Player[refe].transform.position, transform.position) < 16)
            {
                agent.SetDestination(Player[refe].transform.position);



            }
        }
    }
    private void OnCollisionEnter(Collision collision)
    {
        if (collision.gameObject.CompareTag("Player"))
        {
            collision.gameObject.transform.position = Punto.position;
        }
    }
}
